console.log("Hello 25th Day")

// postman - debugging tools

// JSON or Javascript Obecject Notation is a data format used by applications to store and transport data to one anotehr

// files extention for JavaScript is "js", and Jason is "json". - String format

// [SECTION] JSON object
/*
	- JSON stands for JavaScript Object Notation
	- JSON is also used in other programming languages
	- Core JavaScript has a built in JSON object that contain methods for passing JSON object
	- JSON is used for serializing different data types into bytes
*/

// JSON Object - Example
/*
	- JSON also uses the "key/value pairs" just like the object properties in JavaScript
	- Note : key/property names should be enclosed with double qoutes " "" ".
	- Syntax
		{
			"propertyA" : "valueA",
			"propertyB" : "valueB",
			"propertyC" : "valueC"
		}
*/

/*
	{
		"city" : "Quezon City",
		'province : "Metro Manila",
		"country" : "Philippines"
	}
*/

// JSON Arrays 
/*
	- Arrays in JSON are almost same as arrays in JavaScript
	- Basically array in JSON will convert into JSON object
*/

/*
	"cities" : [
		{
		"city" : "Quezon City",
		"province" : "Metro Manila",
		"country" : "Philippines"
		},
		{
		"city" : "Manila City",
		"province" : "Metro Manila",
		"country" : "Philippines"
		},
		{
		"city" : "Makati City",
		"province" : "Metro Manila",
		"country" : "Philippines"
		}
	]

*/

// [SECTION] JSON methods
// The "JSON Objects" containes method for parsing and converting data in stringified JSON
// JSON data is sent or received in text only(String (" ")) format

// Converting Data Intro Stringigied JSON

let batchesArr = [
	{
		batchName : 230,
		schedule : "Part Time"
	},
	{
		batchName : 240,
		schedule : "Full Time"
	}
];
console.log(batchesArr);
console.log("Result from stringify method: ");
// Syntax : JSON.stringify(arrayName/objectName)
console.log(JSON.stringify(batchesArr));

let data = JSON.stringify(
{
	name : "John",
	age : 31,
	address : {
		city : "Manila",
		country : "Phillippines"
	}
}
)
console.log(data);

// User detailts 

/*
let firstName = prompt("Enter your first name: ");
let lastName = prompt("Enter your last name: ");
let email = prompt("Enter your email: ");
let password = prompt("Enter your password");

let account = JSON.stringify(
{ //propertyName / variableName
	firstName : firstName,
	lastName : lastName,
	email : email,
	password : password
	
})
console.log(account);
*/

// Converting Stringified JSON into JavaScript Object

let batchesJSON = `
[
	{
		"batchName" : 230,
		"schedule" : "Part Time"
	},
	{
		"batchName" : 240,
		"schedule" : "Full Time"
	}

]`;

console.log("batchesJSON content: ");
console.log(batchesJSON);

console.log("Result from parse method: ")
let parseBatches = JSON.parse(batchesJSON);
console.log(parseBatches);
console.log(parseBatches[0].batchName);


let stringifiedObject = `
{
	"name" : "John",
	"age" : 31,
	"address" : {
		"city" : "Manila",
		"country" : "Phillippines"
	}
}
`
console.log(stringifiedObject); // Goal is to make this an object of a JavaScript

// console.log(JSON.parse(stringifiedObject))
let originalObject = JSON.parse(stringifiedObject);
console.log(originalObject);
// console.log(originalObject[0].batchName);

























































